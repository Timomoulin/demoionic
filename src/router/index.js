import { createRouter, createWebHistory } from '@ionic/vue-router';



const routes = [
  {
    path: '/',
    redirect: '/memories'
  },

  {
    path:'/memories',
    name:'Memories',
    component:()=>import("../views/SweetSweetMemories.vue")
  },
   {
      path: '/memories/:id',
      component:()=> import ('../views/MemoryDetails.vue')
     },
     {
      path: '/memories/add',
      component:()=> import ('../views/AddMemory.vue')
     }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
