import { defineStore } from 'pinia'

export const memoryStore = defineStore('memoryStore', {

 state: () => {
  return { memories:[
    {id:1,titre:"Le sapin en Java", description:"Nos débuts en Java",image:"https://www.sapins-noel.fr/99/37.jpg"},
    {id:2,titre:"La Todo List", description:"Les servlets",image:"https://i.stack.imgur.com/KA81b.png"},
    {id:3,titre:"Les ampoules", description:"Initiation a Spring et l'injection de dépendance",image:"https://www.360m2.fr/sites/default/files/couv_51.jpg"},
    {id:4,titre:"L'API Héros", description:"Démonstration d'une API Rest et creation de projets Vue.js",image:"https://jcsatanas.fr/wp-content/uploads/2016/10/donjon-de-naheulbeuk-un-remede-a-la-morosite.jpg"},
  ] }
 },
 getters:{
 
  memory(state){
    return (memoryId)=>{
      return state.memories.find((unSouvenir)=>unSouvenir.id==memoryId)
    }
  }
 },

 actions:{
  addMemory(memory){
    this.memories.push({id:this.memories.length,...memory});
  }
 }

})